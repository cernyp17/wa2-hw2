<%@ page import="wa2.repository.ICarRepository" %>
<%@ page import="wa2.repository.CarRepositoryImpl" %>
<%@ page import="wa2.entities.Manufacturer" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: cerny
  Date: 29.03.2016
  Time: 15:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>InsertCar</title>
</head>
<body>
<form action="/insertcar">
    <input name="name">
    <select name="manufacturer_id">
        <%
            ICarRepository repository = new CarRepositoryImpl();
            List<Manufacturer> manufacturers = repository.findAllManfucturers();

            for (Manufacturer manufacturer : manufacturers
                    ) {
                out.write("<option value=\""+manufacturer.getId()+"\">"+manufacturer.getName()+"</option>\n");
            }


        %>
    </select>
    <input type="submit" value="Submit">
</form>
</body>
</html>
