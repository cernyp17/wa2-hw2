<%@ page import="wa2.repository.ICarRepository" %>
<%@ page import="wa2.repository.CarRepositoryImpl" %>
<%@ page import="java.util.List" %>
<%@ page import="wa2.entities.Car" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="wa2.entities.Manufacturer" %><%--
  Created by IntelliJ IDEA.
  User: cerny
  Date: 29.03.2016
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List all cars</title>
</head>
<body>
<a href="/index.jsp"> < back</a>
<form action="listcars.jsp"> <!--address on which forward -->
    <select name="item">
        <%
            ICarRepository repository = new CarRepositoryImpl();
            List<Manufacturer> manufacturers = repository.findAllManfucturers();

            for (Manufacturer manufacturer : manufacturers
                    ) {
                out.write("<option value=\""+manufacturer.getName()+"\">"+manufacturer.getName()+"</option>\n");
            }


        %>
    </select>
    <input type="submit" value="Submit">
</form>
<%
    if (request.getParameter("item")!= null) {
    List<Object[]> list = repository.findAllCarsWithMaterialFromTheirManufacturersSupplier(request.getParameter("item"));
        for (Object[] arr : list) {
            out.write(Arrays.toString(arr)+"<br>");
        }
    } else {
        out.write("select some brand first");
    }
%>
</body>
</html>
