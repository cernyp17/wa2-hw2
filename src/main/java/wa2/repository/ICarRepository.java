package wa2.repository;


import wa2.entities.Car;
import wa2.entities.Manufacturer;

import java.util.List;

/**
 * Created by cerny on 16.03.2016.
 */
public interface ICarRepository {

    List<Car> findAllCars();

    void saveNewCar(Car car);

    List<Object[]> findAllCarsWithMaterialFromTheirManufacturersSupplier(String manufacturerName);

    List<Manufacturer> findAllManfucturers();

    Manufacturer findOneManufacturer(Long id);

}
