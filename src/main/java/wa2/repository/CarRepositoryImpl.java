package wa2.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import wa2.entities.Car;
import wa2.entities.Manufacturer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by cerny on 23.03.2016.
 */
public class CarRepositoryImpl implements ICarRepository {

    DatabaseImpl db = new DatabaseImpl();

    @Override
    public List<Car> findAllCars() {
        final Session session = db.getSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Car c");

        List<Car> results = query.list();
        tx.commit();

        return results;
    }

    @Override
    public void saveNewCar(Car car) {
        final Session session = db.getSession();
        try {
            Transaction tx = session.beginTransaction();
            session.save(car);
            session.flush();

            tx.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public List<Object[]> findAllCarsWithMaterialFromTheirManufacturersSupplier(String manufacturerName) {
        final Session session = db.getSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("select car.name, man.name, sup.name, mat.name from Car car LEFT JOIN car.manufacturer man LEFT JOIN" +
                " man.suppliers sup LEFT JOIN sup.materials mat where man.name = :name");
        query.setParameter("name",manufacturerName);

        List<Object[]> results = query.list();
        tx.commit();

        return results;
    }

    @Override
    public List<Manufacturer> findAllManfucturers() {
        final Session session = db.getSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Manufacturer m");

        List<Manufacturer> results = query.list();
        tx.commit();

        return results;
    }

    @Override
    public Manufacturer findOneManufacturer(Long id) {
        Manufacturer manufacturer;
        final Session session = db.getSession();
        try {
            Transaction tx = session.beginTransaction();
            manufacturer = (Manufacturer) session.get(Manufacturer.class, id);

            tx.commit();
        } finally {
            session.close();
        }
        return manufacturer;
    }

}
