package wa2.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by cerny on 24.03.2016.
 */
@Entity
@DiscriminatorValue(value = "truck")
public class CarTruck extends Car {

    @Column(name = "load_capacity")
    private int loadCapacity;

    public int getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(int loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public CarTruck(String name, Manufacturer manufacturer, int loadCapacity) {
        super(name, manufacturer);
        this.loadCapacity = loadCapacity;
    }

    public CarTruck() {
    }
}
