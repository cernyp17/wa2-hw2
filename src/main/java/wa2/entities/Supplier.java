package wa2.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cerny on 25.03.2016.
 */
@Entity
@Table(name = "supplier")
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "supplier_id")
    private long id;

    private String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "suppliers")
    @Fetch(FetchMode.SELECT)
    @Cascade(CascadeType.SAVE_UPDATE)
    private Set<Manufacturer> manufacturers;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "suppliers")
    @Fetch(FetchMode.SELECT)
    @Cascade(CascadeType.SAVE_UPDATE)
    private Set<Material> materials;

    public void addManufacturer(Manufacturer manufacturer) {
        if (getManufacturers() == null) {
            setManufacturers(new HashSet<Manufacturer>());
        }
        if (manufacturer.getSuppliers() == null){
            manufacturer.setSuppliers(new HashSet<Supplier>());
        }
        getManufacturers().add(manufacturer);
        manufacturer.getSuppliers().add(this);
    }

    public void addMaterial(Material material){
        if (getMaterials() == null) {
            setMaterials(new HashSet<Material>());
        }
        if (material.getSuppliers() == null){
            material.setSuppliers(new HashSet<Supplier>());
        }
        getMaterials().add(material);
        material.getSuppliers().add(this);
    }

    public Set<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(Set<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Supplier(String name) {
        this.name = name;
    }

    public Supplier() {
    }


}
