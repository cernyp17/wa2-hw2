package wa2.servlets;

import wa2.entities.Car;
import wa2.repository.CarRepositoryImpl;
import wa2.repository.ICarRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/cars")
public class GetCars extends HttpServlet {

    private ICarRepository repository = new CarRepositoryImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Car> cars = repository.findAllCars();

        response.getWriter().write("<html><head></head><body>" +
                "<a href=\"/index.jsp\"> < back</a><br>");
        for(Car car : cars){
            response.getWriter().write(car.toString() + "<br>");
        }
        response.getWriter().write("</body></html>");

    }
}
