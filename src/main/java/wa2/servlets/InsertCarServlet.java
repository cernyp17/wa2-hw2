package wa2.servlets;

import com.google.common.collect.Sets;
import wa2.entities.Car;
import wa2.entities.Manufacturer;
import wa2.entities.Supplier;
import wa2.repository.CarRepositoryImpl;
import wa2.repository.ICarRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/insertcar")
public class InsertCarServlet extends HttpServlet {

    private ICarRepository repository = new CarRepositoryImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Car car = new Car();
        car.setName(req.getParameter("name"));
        Manufacturer man = repository.findOneManufacturer(Long.parseLong(req.getParameter("manufacturer_id")));
        car.setManufacturer(man);

        repository.saveNewCar(car);

        resp.getWriter().write("<html><head></head><body>" +
                "<a href=\"/index.jsp\"> < back</a> <br>" +
                "Car saved" +
                "</body></html>");
    }
}


