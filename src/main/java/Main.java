import com.google.common.collect.Sets;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import wa2.entities.*;

import java.util.*;

/**
 * Created by Martin on 4. 3. 2015.
 */
public class Main {
    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        System.out.println("hello world");
        final Session session = getSession();
        try {
            Transaction tx = session.beginTransaction();
            //--------HW2-----------------
            //create manufacturer
            Manufacturer skoda = new Manufacturer("Skoda");
            //create cars
            Car octavia = new Car("Octavia", skoda);
            CarTruck pickup = new CarTruck("Pickup", skoda, 100);

            session.save(octavia);
            session.save(pickup);

            //create customer
            Customer customer = new Customer("Pavel");

            HashSet<Phone> phones = new HashSet<Phone>();
            phones.add(new Phone(PhoneType.MOBILE, customer));
            phones.add(new Phone(PhoneType.WORK, customer));

            HashSet<Address> addresses = new HashSet<Address>();
            addresses.add(new Address("Praha", "Rezlerova", "272", customer));

            customer.setPhones(phones);
            customer.setAddresses(addresses);
            //save customer
            session.save(customer);

            //create borrowing
            Borrowing borrowing = new Borrowing(customer,pickup);
            session.save(borrowing);

            session.flush();
            tx.commit();
            //------------HW3----------
            tx = session.beginTransaction();
            Supplier ocelarnyTrinec = new Supplier("ocelarny Trinec");
            Supplier sklarnyJablonec = new Supplier("sklarny Jablonec");
            Supplier gumarny = new Supplier("gumarny");
            Supplier plastSmer = new Supplier("plastove hmoty Smer");

            Material ocel = new Material("ocel");
            Material ocelovePiliny = new Material("ocelove piliny");
            Material sklo = new Material("sklo");
            Material pneumatiky = new Material("pneumatiky");

            ocelarnyTrinec.addMaterial(ocel);
            ocelarnyTrinec.addMaterial(ocelovePiliny);

            sklo.setSuppliers(Sets.<Supplier>newHashSet(sklarnyJablonec));
            pneumatiky.setSuppliers(Sets.<Supplier>newHashSet(gumarny,plastSmer));

            session.save(ocelarnyTrinec);
            session.save(pneumatiky);
            session.save(sklo);

            session.flush();
            tx.commit();

            System.out.println("----insert use case----");
            System.out.println("--Vlož nové auto s novým výrobcem, který má některé dodavatele kteří již existují a některé nové dodavatele -- ");
            tx = session.beginTransaction();

            Manufacturer fiat = new Manufacturer("Fiat");
            Car punto = new Car("Punto",fiat);
            Supplier zelezarstviRousek = new Supplier("zelezarstvi Rousek");
            fiat.setSuppliers(Sets.<Supplier>newHashSet(ocelarnyTrinec,zelezarstviRousek));
            zelezarstviRousek.setMaterials(Sets.newHashSet(ocelovePiliny));

//            session.save(zelezarstviRousek);
            session.save(punto);


            session.flush();
            tx.commit();

            System.out.println("----insert use case 2----");
            System.out.println("-- vlož nového výrobce s existujícím dodavatelem, který má nový materiál  --");
            tx = session.beginTransaction();

            Manufacturer bmw = new Manufacturer("BMW");
            sklarnyJablonec.addManufacturer(bmw);
            Material celnisklo = new Material("celni sklo");
            sklarnyJablonec.addMaterial(celnisklo);
            session.save(bmw);

            session.flush();
            tx.commit();

            System.out.println("----update use case 1----");
            System.out.println("-- Autu s Id změň výrobce, kterému zároveň změníte složení dodavatelů  --");
            tx = session.beginTransaction();

            Car changedCar = (Car) session.get(Car.class, pickup.getId());
            bmw.getSuppliers().add(ocelarnyTrinec);
            changedCar.setManufacturer(bmw);
            session.update(changedCar);

            session.flush();
            tx.commit();

            System.out.println("----update use case 2----");
            System.out.println("-- výpujčce s id změň adresu zákazníka a typ auta  na punto--");
            tx = session.beginTransaction();

            Borrowing changedBorrwing = (Borrowing) session.get(Borrowing.class,1L);
            changedBorrwing.setCar(punto);
            Set<Address> changedAdresses = changedBorrwing.getCustomer().getAddresses();
            for (Address address : changedAdresses
                 ) {
                if (address.getCity().equalsIgnoreCase("Praha")) {
                    address.setCity("Brno");
                }
            }
            session.update(changedBorrwing);


            session.flush();
            tx.commit();

            System.out.println("----select use case 1----");
            System.out.println("-- Vyber a vypiš všechna auta od výrobce Fiat a k nim napiš seznam veškerého materiálu od všech dodavatelů pro Fiat--");
            tx = session.beginTransaction();

            Query query = session.createQuery("from Car c where manufacturer_id = :id");
            query.setParameter("id", fiat.getId());
            List<Car> results = query.list();

            for (Car car : results
                    ) {
                System.out.println("-----car: " + car.getName() + " manufacturer:" + car.getManufacturer().getName());
            }

            query = session.createQuery("select car.name, man.name, mat.name from Material mat "
                    + "JOIN mat.suppliers sup JOIN sup.manufacturers man JOIN man.cars car where man.name = :name");
            query.setParameter("name","Fiat");
            List<Object[]> list = query.list();
            for(Object[] arr : list){
                System.out.println("-----" + Arrays.toString(arr));
            }

            session.flush();
            tx.commit();

            System.out.println("----select use case 2----");
            System.out.println("-- Vyber a vypiš všechny telefony, od osob, co mají zapůjčeny osobní auta--");
            tx = session.beginTransaction();

            query = session.createQuery("select pho.phoneType, cus.name, car.name from Phone pho JOIN pho.customer cus" +
                    " JOIN cus.borrowings bor JOIN bor.car car WHERE car.class = :type");
            query.setParameter("type","car");
            List<Object[]> list2 = query.list();
            for(Object[] arr : list2){
                System.out.println(Arrays.toString(arr));
            }

            session.flush();
            tx.commit();



            System.out.println("bye world");
        } finally {
            session.close();
        }
    }



}
